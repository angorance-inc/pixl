# PixL
PixL is a social network aimed to the photographers, regardless of the experience. It's a place where one can share his view of the world, ask for advices, and dispense his expertise with other photography enthusiasts. 

## Contributors

* **Bryan Curchod** - *Backend / Database* - [BryCur](https://gitlab.com/BryCur)
* **Daniel Gonzalez Lopez** - *Frontend* - [Angorance](https://gitlab.com/Angorance)
* **Héléna Reymond** - *Frontend / Database* - [LNAline](https://gitlab.com/LNAline)

## Technologies
### Frontend
For the frontend, we chose to user [Vue.js](https://vuejs.org). As during the semester we had the occasion to try Angular and React, it seems natural to try the last "big boss" of the front-end frameworks.
### Backend
For the server-side, we opted for [Nest.js](https://nestjs.com), it is a recent framework based on the Typescript language. We had some good experience with this language, so we thought that we should give this framework a try.
### Database
We chose [ElasticSeach](https://www.elastic.co/fr/products/elasticsearch) to manage our data because it seemed very interesting to use when it came to the research in the documents (a point that can be handy in a social network). 

### API
To build the API, we could chose between using GraphQL or building a REST API.
The Three (3) main technologies mentioned above were all new to us, and we didn't want to take too much risk as we didn't practice GraphQL, so the REST API was our choice.

N.B. : you can check our API documentation by following [this link](https://pixl.tech/api/doc/)

### Tests
To test our application, we used Chai and Mocha to implement unit tests in the backend.

### Deployment
To deploy our application, we chose to host the different tiers (front-/back-end and database) on a virtual machine on Debian 9 provided by the the Google Cloud Platform. We implemented a Docker container for each tier, and managed these container with a docker-compose file.

In addition to our tiers, and to secure our application (SSL certificate) we implemented a container Traefik that will act as a reverse proxy and manage a Let's Encrypt certificate. In other words, a container Traefik will receive the queries aiming at `pixl.tech`, and will then dispatch them on the frontend or the backend, base on the target URL. On the other hand, the database isn't known by Traefik, only the backend has a direct access to it.

For the frontend container, we had to chosea `nginx` container taht will redirect every query to the base URL `/`, Vue.js will take care of the routing.

## User Manual
The first step to use our social network is obviously to create your personal account via the signup form, and then to log into it (via the login form).

When you're connected, you'll land to the main page of PixL, you wall. In this place you'll see the last posts published by your fellow Pixler (that's how we call our members). If it's your very first connection, we highly recommend you to configure your profile on your personal page, the button leading there is in the navigation bar at the top of the site.

On you personal page you'll see your own posts, and can set your favorite tags, as well as your personal information (such as your location, a blog, or a biography).

As the project is still in pre-alpha, we beg your pardon for the lack of personalisation or research.

## Self-report
We don't think that we did a bad work, but we definitely could do way better. Obviously such a short notice for such a big project was a prime reason for the "failure" of the project, not to mention the time needed to take all the new technologies in hand. But we think that we may have bitten off more than we could chew, we underestimated the time needed for a project like this and we ended up with many little troublesome details that we didn't think of in the first place.

Also we think that it's kind of sad that we didn't have a proper lesson on how a real social network such as Twitter or Instagram work, how the datas are handled, etc.

However this project surely was an exceptional experience, not only for our career, but for our personal development as well.

