import { ApiModelProperty } from '@nestjs/swagger';

export class UserAccountDTO {
    @ApiModelProperty()
    readonly fName: string;

    @ApiModelProperty()
    readonly lName: string;

    @ApiModelProperty()
    readonly email: string;

    @ApiModelProperty()
    readonly password: string;

    @ApiModelProperty()
    readonly birthdate: string;

    @ApiModelProperty()
    readonly biography: string;

    @ApiModelProperty()
    readonly localisation: string;

    @ApiModelProperty()
    readonly website: string;

    @ApiModelProperty()
    readonly profilePic: string;

    @ApiModelProperty()
    readonly tagList: string[];

    @ApiModelProperty()
    readonly followingList: string[];
}