import { ApiModelProperty } from '@nestjs/swagger';

export class LoginRequest {
    @ApiModelProperty()
    password: String;
    @ApiModelProperty()
    readonly username: String;
}