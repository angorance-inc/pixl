import { ApiModelProperty } from '@nestjs/swagger';

export class CreateAccountDTO {
    @ApiModelProperty()
    readonly fName: string;

    @ApiModelProperty()
    readonly lName: string;

    @ApiModelProperty()
    readonly email: string;

    @ApiModelProperty()
    password: string;

    @ApiModelProperty()
    readonly username: string;

    profilePic: string;
}