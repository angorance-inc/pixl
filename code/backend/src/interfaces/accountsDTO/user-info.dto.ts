import { ApiModelProperty } from "@nestjs/swagger";

export class UserInfo {
    @ApiModelProperty()
    readonly userID: String;

    @ApiModelProperty()
    readonly username: String;
}