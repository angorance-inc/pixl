import { ApiModelProperty } from '@nestjs/swagger';

export class LoginSuccess {
    @ApiModelProperty()
    readonly token: String;
}