import { ApiModelProperty } from '@nestjs/swagger';
import { CreateAccountDTO } from '../accountsDTO/create-account.dto';

export class profileDTO extends CreateAccountDTO {

    @ApiModelProperty()
    readonly birthdate: Date;

    @ApiModelProperty()
    readonly biography: string;

    @ApiModelProperty()
    readonly localisation: string;

    @ApiModelProperty()
    readonly website: string;

    @ApiModelProperty()
    readonly profilePic: string;

    @ApiModelProperty()
    readonly tagList: string[] = [];
}