export class ProfileMessages {
    static readonly IdsUnmatched: string = 'You are not allowed to do this operation !';
    static readonly AlreadyFollowed: string = 'id to follow already in the subscription list';
    static readonly DoesntFollow: string = 'id to unfollow not found in subscription list'
}