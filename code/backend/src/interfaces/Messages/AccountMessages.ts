export class AccountMessages {
    // Error messages 
    static readonly mailUsernameInUse: string = 'mail or username already in use !';
    static readonly cantCreateUser: string = 'unable to create the new user';
    static readonly badCredentials: string = 'bad credentials';
    static readonly accountNotActivated: string = 'This account hasn\'t been activated yet';

    // Success messages
    static readonly registrationSuccess: string = 'registration OK';
    static readonly authenticationSuccess: string = 'authentication OK';
}