export class GenericMessages {
    // Error messages 
    static readonly fieldMissing: string = 'a field is missing!';
    static readonly invalidToken: string = 'The token provided is invalid!';
}