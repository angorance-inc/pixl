export class PublicationMessages {
    // Error messages 
    static readonly NotFound: string = 'The publication requested has not been found...';
}