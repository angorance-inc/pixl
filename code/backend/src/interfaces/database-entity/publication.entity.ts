export class PublicationEntity {
    creatorID: string;
    creatorUsername: string;
    title: string;
    description: string;
    tags: string[];
    image: string;
    publicationDate: number;
}