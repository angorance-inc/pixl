export class AccountEntity {
    fName: string;
    lName: string;
    email: string;
    username: string;
    password: string;
    birthdate: string;
    biography: string;
    localisation: string;
    website: string;
    profilePic: string;
    tagList: string[];
    followingList: string[];
}