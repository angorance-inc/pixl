import { ApiModelProperty } from '@nestjs/swagger';

export class PublicationDTO {

    @ApiModelProperty()
    readonly creatorID: string;

    @ApiModelProperty()
    readonly creatorUsername: string;

    @ApiModelProperty()
    readonly title: string;

    @ApiModelProperty()
    readonly description: string;

    @ApiModelProperty()
    readonly tags: string[];

    @ApiModelProperty()
    readonly image: string;

    publicationDate: number;

}