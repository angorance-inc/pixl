import { ApiModelProperty } from "@nestjs/swagger";

export class PublicationLightPersonal {
    @ApiModelProperty()
    id: string;

    @ApiModelProperty()
    title: string;

    @ApiModelProperty()
    description: string;

    @ApiModelProperty()
    image: string;

    @ApiModelProperty()
    creatorUsername: string;

    @ApiModelProperty()
    creatorId: string;

    @ApiModelProperty()
    tags: string[];
}