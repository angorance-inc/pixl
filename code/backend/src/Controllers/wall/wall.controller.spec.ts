import { Test, TestingModule } from '@nestjs/testing';
import { WallController } from './wall.controller';

describe('Wall Controller', () => {
  let module: TestingModule;
  
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [WallController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: WallController = module.get<WallController>(WallController);
    expect(controller).toBeDefined();
  });
});
