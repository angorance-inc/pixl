import { Controller, Get, HttpException, HttpStatus, Param, Header, Headers, Query, Body } from '@nestjs/common';
import { ConfigService } from '../../configs/ConfigService';
import { GenericMessages } from '../../interfaces/Messages/genericMessages';
import * as jwt from 'jsonwebtoken';
import { queryHandler as db } from '../../ElasticSearch/queryHandler';
import {
    ApiUseTags,
    ApiOkResponse,
    ApiConsumes,
    ApiProduces,
    ApiBadRequestResponse,
    ApiForbiddenResponse,
    ApiCreatedResponse,
    ApiImplicitQuery,
    ApiImplicitHeader
} from '@nestjs/swagger';
import { PublicationLightPersonal } from '../../interfaces/publications/publication-light.dto';
import { QueryHitDTO } from '../../interfaces/publications/query-hit.dto.';
import { PublicationDTO } from '../../interfaces/publications/publication.dto';

@Controller('wall')
@ApiUseTags('wall')
export class WallController {
    constructor(private readonly config: ConfigService) { }

    /**
    * Endpoint : GET /wall/home
    * get publications depending on the user's tag favorites, or his/her query terms
    */
    @Get()
    @ApiProduces('application/json')
    @ApiOkResponse({ description: "array of publication", type: PublicationLightPersonal })
    @ApiForbiddenResponse({ description: GenericMessages.invalidToken })
    @ApiImplicitQuery({ description: 'page number to fetch', type: 'int', name: 'page', required: true })
    @ApiImplicitQuery({ description: 'how many publications to get', type: 'int', name: 'limit', required: true })
    @ApiImplicitHeader({ name: 'authorization', description: 'authorisation token', required: true })
    async getHomeWall(@Body() tagList: string[], @Headers('authorization') token: string, @Query("page") page: number, @Query('limit') limit: number) {
        console.log('GET /wall/home - requesting home page wall');

        // check token
        this.verifyToken(token);

        // TODO refine the research with user's tags / query terms

        // fetch the posts
        let publicationList = await db.GetPublicationsByResearch(page, limit);

        console.log('LIST LENGTH : ' + publicationList.length);

        let minimalPublicationList = this.getMinimalPostList(publicationList);

        return minimalPublicationList;
    }

    /**
     * Endpoint : GET /wall/:userId
     * get every publication posted by a user
     */
    @Get(':userId')
    @ApiProduces('application/json')
    @ApiOkResponse({ description: "array of publication", type: PublicationLightPersonal })
    @ApiForbiddenResponse({ description: GenericMessages.invalidToken })
    @ApiImplicitQuery({ description: 'page number to fetch', type: 'int', name: 'page', required: true })
    @ApiImplicitQuery({ description: 'how many publications to get', type: 'int', name: 'limit', required: true })
    @ApiImplicitHeader({ name: 'authorization', description: 'authorisation token', required: true })
    async getPersonalWall(@Param('userId') userId: string, @Headers('authorization') token: string, @Query("page") page: number, @Query('limit') limit: number) {
        console.log('GET /wall/:userId - requesting personal wall for user ' + userId);

        // check token
        this.verifyToken(token);


        // fetch the post from the specified user
        let publicationList = await db.GetPublicationsByUserID(userId, page, limit);

        console.log('LIST LENGTH : ' + publicationList.length);

        let minimalPublicationList = this.getMinimalPostList(publicationList);

        return minimalPublicationList;
    }
    getMinimalPostList(publicationList: QueryHitDTO<PublicationDTO>[]): PublicationLightPersonal[] {
        return publicationList.map(p => {
            // data sent to the client
            return {
                id: p._id,
                title: p._source.title,
                description: p._source.description,
                image: p._source.image,
                creatorUsername: p._source.creatorUsername,
                creatorId: p._source.creatorID,
                tags: p._source.tags
            }
        });
    }



    /**
     * validate a token, throw an error if the token provided is invalid 
     * @param token token to validate
     */
    verifyToken(token: string) {
        // check the token provided
        // TODO test with a validated token
        try {
            jwt.verify(token, this.config.get('SECRET'));
            /*if (token && this.jwt.verify(token)) {
                return;
            }*/
        } catch (error) {
            console.log(`error while verifying the token : ${error}`);
            console.log('token invalid');

            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: GenericMessages.invalidToken
            }, 403);
        }

    }
}
