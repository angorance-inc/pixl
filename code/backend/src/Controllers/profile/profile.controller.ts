import { Controller, Post, Body, Headers, Get, Param, Delete, Query, HttpException, HttpStatus } from '@nestjs/common';
import { profileDTO } from '../../interfaces/profilesDTO/profile.dto';
import { UserAccountDTO } from '../../interfaces/accountsDTO/user-account.dto';
import { GenericMessages } from '../../interfaces/Messages/genericMessages';
import * as jwt from 'jsonwebtoken';
import { ConfigService } from '../../configs/ConfigService';
import { queryHandler as db } from '../../ElasticSearch/queryHandler';
import { ProfileMessages } from '../../interfaces/Messages/ProfileMessages';
import { AccountEntity } from '../../interfaces/database-entity/account.entity';
import {
    ApiForbiddenResponse, ApiConsumes, ApiImplicitParam, ApiImplicitQuery,
    ApiImplicitHeader, ApiOkResponse, ApiCreatedResponse, ApiProduces, ApiUseTags
} from '@nestjs/swagger';

@Controller('profiles')
@ApiUseTags('Profile')
export class ProfileController {
    constructor(private readonly config: ConfigService) { }

    /**
     * get the subscription list of a specified user
     * @param userId Id of the user to get the subscription list from
     * @param token authentication token
     */
    @Get('/subs/:userId')
    @ApiProduces('application/json')
    @ApiForbiddenResponse({ description: GenericMessages.invalidToken })
    @ApiOkResponse({ description: 'Array of followed user\'s ids' })
    @ApiImplicitHeader({ name: 'authorization', description: 'authorisation token', required: true })
    @ApiImplicitParam({
        name: 'userId', description: 'id of the user from which we want the subscription list',
        required: true, type: 'string'
    })
    async getSubsciptionList(@Param('userId') userId: string, @Headers('authorization') token: string) {
        console.log('GET /subs/:userid : get ' + userId + ' user\'s subscription list');

        this.verifyToken(token);
        // fetch subscription list
        const user = await db.getUserProfile(userId);

        if (user.followingList) {
            return user.followingList;
        } else {
            return [];
        }
    }

    /**
     * Allows an user to unsubscribe to another user
     * @param subscriber ID of the user that subscribes to another 
     * @param followedId ID of the user to remove from the subscription list
     * @param token authentication token
     */
    @Delete('/subs/:userId')
    @ApiProduces('plain/text')
    @ApiForbiddenResponse({ description: GenericMessages.invalidToken })
    @ApiForbiddenResponse({ description: ProfileMessages.IdsUnmatched })
    @ApiOkResponse({ description: 'operation confirmation' })
    @ApiImplicitHeader({ name: 'authorization', description: 'authorisation token', required: true })
    @ApiImplicitParam({
        name: 'userId',
        description: 'id of the user from which we want to remove an element in the subscription list',
        required: true,
        type: 'string'
    })
    @ApiImplicitQuery({ name: 'followedId', description: 'id of the user to unfollow', required: true, type: 'string' })
    async unfollow(@Param('userId') subscriber: string, @Query('followedId') followedId: string, @Headers('authorization') token: string) {
        console.log('DELETE /subs/:userid : user ' + subscriber + " now follows " + followedId);

        // check token validity
        this.verifyToken(token);

        // check that the subscriber ID is the same as the id in the token (ensure that it's the same person)
        this.checkIdentity(token, subscriber);

        // get the user's subscription list
        const userData: AccountEntity = await db.getUserProfile(subscriber);


        // check that the subscription to delete is in the list
        if (!userData.followingList) {
            userData.followingList = [];
        }

        const index: number = userData.followingList.indexOf(followedId);
        if (index === -1) {
            console.log(ProfileMessages.DoesntFollow);
            return ProfileMessages.DoesntFollow;
        }

        // delete the requested subscription for the user
        userData.followingList.splice(index, 1);
        await db.updateProfile(subscriber, { followingList: userData.followingList })

        return "successfully unfollowed";
    }

    /**
     * Allows an user to subscribe to another user
     * @param subscriber ID of the user that subscribes to another 
     * @param followedId ID of the user to add to the subscription list
     * @param token authentication token
     */
    @Post('/subs/:userId')
    @ApiProduces('plain/text')
    @ApiForbiddenResponse({ description: GenericMessages.invalidToken })
    @ApiForbiddenResponse({ description: ProfileMessages.IdsUnmatched })
    @ApiCreatedResponse({ description: 'operation confirmation' })
    @ApiImplicitHeader({ name: 'authorization', description: 'authorisation token', required: true })
    @ApiImplicitParam({
        name: 'userId',
        description: 'id of the user from which we want to add an element in the subscription list',
        required: true,
        type: 'string'
    })
    @ApiImplicitQuery({ name: 'followedId', description: 'id of the user to follow', required: true, type: 'string' })
    async follow(@Param('userId') subscriber: string, @Query('followedId') followedId: string, @Headers('authorization') token: string) {
        console.log('POST /subs/:userid : user ' + subscriber + " now follows " + followedId);

        // check token validity
        this.verifyToken(token);

        // check that the subscriber ID is the same as the id in the token (ensure that it's the same person)
        this.checkIdentity(token, subscriber)

        // get the user's subscription list
        const userData: AccountEntity = await db.getUserProfile(subscriber);

        // check that the subscription isn't already in the list
        if (!userData.followingList) {
            userData.followingList = [];
        } else if (userData.followingList.indexOf(followedId) !== -1) {
            console.log(ProfileMessages.AlreadyFollowed);
            return ProfileMessages.AlreadyFollowed;
        }

        // add the requested subscription for the user
        userData.followingList.push(followedId)
        await db.updateProfile(subscriber, { followingList: userData.followingList })

        return "OK";
    }

    /**
     * Endpoint: POST /profiles/profile
     * Allow a user to update/post his profile
     * @param profileData user profile
     * @param token authorization token, no token, no action
     */
    @Post(':userId')
    @ApiProduces('plain/text')
    @ApiConsumes('application/json')
    @ApiForbiddenResponse({ description: GenericMessages.invalidToken })
    @ApiForbiddenResponse({ description: ProfileMessages.IdsUnmatched })
    @ApiCreatedResponse({ description: 'operation confirmation' })
    @ApiImplicitHeader({ name: 'authorization', description: 'authorisation token', required: true })
    @ApiImplicitParam({
        name: 'userId',
        description: 'id of the user\'s profile to update',
        required: true,
        type: 'string'
    })
    async postProfile(@Param('userId') userId: string, @Body() profileData: profileDTO, @Headers('authorization') token: string) {
        console.log('POST /:userId : New profile data : ' + JSON.stringify(profileData) + '\n Token: ' + token);

        // check token validity
        this.verifyToken(token);

        // check identity (only the possessor of the profile can edit it)
        this.checkIdentity(token, userId);

        db.updateProfile(userId, profileData);

        return 'profile updated';
    }

    /**
     * Endpoint: GET /profiles/profile
     * Allow a user to retrieve his profile
     * @param token authorization token, no token, no action
     */
    @Get(':userId')
    @ApiProduces('application/json')
    @ApiForbiddenResponse({ description: GenericMessages.invalidToken })
    @ApiOkResponse({ description: 'profile data', type: UserAccountDTO })
    @ApiImplicitHeader({ name: 'authorization', description: 'authorisation token', required: true })
    @ApiImplicitParam({
        name: 'userId',
        description: 'id of the user\'s profile requested',
        required: true,
        type: 'string'
    })
    async getProfile(@Param('userId') userId: string, @Headers('authorization') token: string) {
        console.log('GET /:userId : Profile recuperation, Token: ' + token);
        this.verifyToken(token);

        // check token validity
        this.verifyToken(token);
        // get profile

        return await db.getUserProfile(userId);
    }


    /**
     * Endpoint: GET /profiles/:userId/randomBanner
     * Retrieve and return a random image from a user's publication
     * @param userId id of the user from which we want a banner
     * @param token authorisation token
     */
    @Get(':userId/randomBanner')
    @ApiProduces('plain/text')
    @ApiForbiddenResponse({ description: GenericMessages.invalidToken })
    @ApiCreatedResponse({ description: 'banner image in base 64', type: 'string' })
    @ApiImplicitHeader({ name: 'authorization', description: 'authorisation token', required: true })
    @ApiImplicitParam({
        name: 'userId',
        description: 'id of the profile from which we pick an image',
        required: true,
        type: 'string'
    })
    async getBanner(@Param('userId') userId: string, @Headers('authorization') token: string) {
        console.log('GET /:userId/randomBanner : banner for ' + userId);

        // check token validity
        this.verifyToken(token);

        // get random image for banner
        return await db.getRandomUserPublication(userId);
    }

    /**
     * validate a token, throw an error if the token provided is invalid 
     * @param token token to validate
     */
    verifyToken(token: string) {
        // check the token provided
        try {
            console.log('verifying : ' + token);

            jwt.verify(token, this.config.get('SECRET'));
        } catch (error) {
            console.log(`error while verifying the token : ${error}`);
            console.log('token invalid');

            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: GenericMessages.invalidToken
            }, 403);
        }
    }

    /**
     * ensure that a user requesting a personal operation on an account (such as
     * a profile update, or a follow/unfollow) is the same than the concerned
     * @param token token provided by the user
     * @param requesterId id of the user concerned by a personal operation
     */
    checkIdentity(token: string, requesterId: string) {
        const tokenContent: any = jwt.decode(token);
        if (tokenContent.userID !== requesterId) {
            console.log('Operation denied - the ids don\'t match');
            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: ProfileMessages.IdsUnmatched
            }, 403);
        }
    }
}

