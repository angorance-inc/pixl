import { Controller, Post, Body, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateAccountDTO } from '../../interfaces/accountsDTO/create-account.dto';
import { AccountMessages } from '../../interfaces/Messages/AccountMessages';
import { LoginRequest } from '../../interfaces/accountsDTO/login-body.dto';
import { queryHandler as db } from '../../ElasticSearch/queryHandler';
import * as jwt from 'jsonwebtoken';
import { ApiBadRequestResponse, ApiForbiddenResponse, ApiInternalServerErrorResponse, ApiConsumes, ApiOkResponse, ApiCreatedResponse, ApiProduces, ApiUseTags } from '@nestjs/swagger';
import { LoginSuccess } from '../../interfaces/accountsDTO/loginSuccess.dto';
import { GenericMessages } from '../../interfaces/Messages/genericMessages';
import { ConfigService } from '../../configs/ConfigService';
import { UserInfo } from 'src/interfaces/accountsDTO/user-info.dto';

const bcrypt = require('bcrypt');
const saltRounds = 10;

@Controller('accounts')
@ApiUseTags('Account')
@Injectable()
export class AccountController {
    defaultProfilePicture: string = 'https://cdn.pixabay.com/photo/2017/07/18/23/23/user-2517433_960_720.png';

    constructor(private readonly config: ConfigService) { }

    /**
     * Endpoint: POST accounts/registration
     * allow to insert a new user in the database if the mail/username doesn't exist
     * @param newUser User to register, JSON format
     */
    @Post('registration')
    @ApiConsumes('application/json')
    @ApiProduces('plain/text')
    @ApiCreatedResponse({ description: AccountMessages.registrationSuccess })
    @ApiBadRequestResponse({ description: GenericMessages.fieldMissing })
    @ApiForbiddenResponse({ description: AccountMessages.mailUsernameInUse })
    @ApiInternalServerErrorResponse({ description: AccountMessages.cantCreateUser })
    async Registration(@Body() newUser: CreateAccountDTO) {
        console.log('Registration :' + JSON.stringify(newUser));

        // field verification
        if (!newUser.email || !newUser.password || !newUser.fName || !newUser.lName || !newUser.username ||
            newUser.email === '' || newUser.password === '' || newUser.fName === '' || newUser.lName === '' || newUser.username === '') {
            console.log('field missing');

            throw new HttpException({
                status: HttpStatus.BAD_REQUEST,
                error: GenericMessages.fieldMissing
            }, 400);
        };

        // Check if user exists
        let userExsists = await db.DoesUserExist(newUser.email, newUser.username);
        console.log(userExsists);

        if (userExsists) {
            console.log('user already exists');

            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: AccountMessages.mailUsernameInUse
            }, 403);
        }

        // If doesn't exists insert new user in database

        // Password hashing
        newUser.password = bcrypt.hashSync(newUser.password, saltRounds);
        newUser.profilePic = this.defaultProfilePicture;

        let userInserted = await db.InsertUser(newUser);
        if (!userInserted) {
            console.log('couldn\'t register the user');

            throw new HttpException({
                status: HttpStatus.INTERNAL_SERVER_ERROR,
                error: AccountMessages.cantCreateUser
            }, 500);
        } else {
            console.log('User created');

            return AccountMessages.registrationSuccess;
        }
    }

    /**
     * Endpoint: POST accounts/authentication
     * Check the user credential, if a match is found in the database, return a token
     * @param loginReq user credentials for a login attempt, JSON format
     */
    @Post('authentication')
    @ApiConsumes('application/json')
    @ApiProduces('application/json')
    @ApiCreatedResponse({ description: 'authentication token', type: LoginSuccess })
    @ApiBadRequestResponse({ description: GenericMessages.fieldMissing })
    @ApiForbiddenResponse({ description: AccountMessages.badCredentials })
    async Authentication(@Body() loginReq: LoginRequest) {
        console.log('login attempt : ' + JSON.stringify(loginReq));

        // field verification
        if (!loginReq.username || !loginReq.password ||
            loginReq.username === '' || loginReq.password === '') {
            throw new HttpException({
                status: HttpStatus.BAD_REQUEST,
                error: GenericMessages.fieldMissing
            }, 400)
        }

        // retrieve the user ID

        let userInfo: UserInfo = await db.Login(loginReq);

        if (userInfo === null) {
            console.log('bad credentials');

            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: AccountMessages.badCredentials
            }, 403)
        }

        // token generation
        let token = jwt.sign(userInfo, this.config.get('SECRET'));
        const respData: LoginSuccess = { token: token };

        return respData;
    }
}
