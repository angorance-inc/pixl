import { Controller, Post, Body, Headers, Get, Param, Patch, HttpException, HttpStatus, Delete } from '@nestjs/common';
import { PublicationDTO } from '../../interfaces/publications/publication.dto';
import { GenericMessages } from '../../interfaces/Messages/genericMessages';
import { PublicationMessages } from '../../interfaces/Messages/PublicationsMessages';
import * as jwt from 'jsonwebtoken';
import { ConfigService } from '../../configs/ConfigService';
import { queryHandler as db } from '../../ElasticSearch/queryHandler';
import {
    ApiUseTags,
    ApiConsumes,
    ApiProduces,
    ApiBadRequestResponse,
    ApiForbiddenResponse,
    ApiCreatedResponse,
    ApiOkResponse,
} from '@nestjs/swagger';
import { QueryHitDTO } from 'src/interfaces/publications/query-hit.dto.';

@Controller('publications')
@ApiUseTags('Publication')
export class PublicationController {

    constructor(private readonly config: ConfigService) { }

    /**
     * Endpoint : POST /publications
     * Store a publication, the picture are stored in Base64 directly in the DB
     * @param publication Publication object to be stored
     * @param token authorization token
     */
    @Post()
    @ApiConsumes('application/json')
    @ApiProduces('plain/text')
    @ApiBadRequestResponse({ description: GenericMessages.fieldMissing })
    @ApiForbiddenResponse({ description: GenericMessages.invalidToken })
    @ApiCreatedResponse({ description: 'Creation confirmation' })
    async postPublication(@Body() publication: PublicationDTO, @Headers('authorization') token: string) {
        console.log('POST /publications : Publication posting ! token provided : ' + token + ' - Publication creator : ' + publication.creatorUsername + '(' + publication.creatorID + ')');

        // validate the token used
        this.verifyToken(token);

        // every fields have to be provided
        if (!publication.title || !publication.creatorUsername || !publication.image || !publication.tags || !publication.creatorID ||
            publication.title === '' || publication.creatorUsername === '' || publication.image === '' || publication.creatorID === '' || publication.tags.length === 0) {
            console.log('field missing');

            throw new HttpException({
                status: HttpStatus.BAD_REQUEST,
                error: GenericMessages.fieldMissing
            }, 400);
        };

        // set the publication date
        publication.publicationDate = new Date().getTime();

        // save the post in DB
        db.CreatePublication(publication);

        return 'OK';
    }

    @Get(':id')
    @ApiProduces('application/json')
    @ApiForbiddenResponse({ description: GenericMessages.invalidToken })
    @ApiBadRequestResponse({ description: PublicationMessages.NotFound })
    @ApiOkResponse({ description: 'Publication requested', type: PublicationDTO })
    async getPublication(@Param('id') id: string, @Headers('authorization') token: string) {
        console.log('GET /publications:id : id requested : ' + id);
        // token verification
        this.verifyToken(token);

        // retrieve the publication
        let qResult: QueryHitDTO<PublicationDTO>[] = await db.getPublicationsById(id);

        // check that we have the publication
        if (qResult.length === 0) {
            throw new HttpException({
                status: HttpStatus.BAD_REQUEST,
                error: PublicationMessages.NotFound
            }, 400);
        }

        return qResult[0]._source;
    }


    /**
     * validate a token, throw an error if the token provided is invalid 
     * @param token token to validate
     */
    verifyToken(token: string) {
        try {
            // check the token provided
            jwt.verify(token, this.config.get('SECRET'));
        } catch (error) {
            console.log(`error while verifying the token : ${error}`);
            console.log('token invalid');

            throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: GenericMessages.invalidToken
            }, 403);
        }


    }
}