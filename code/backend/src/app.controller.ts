import { Controller, Get, Injectable } from '@nestjs/common';
import { ApiProduces, ApiOkResponse } from '@nestjs/swagger';
import { ConfigService } from './configs/ConfigService';

@Controller()
export class AppController {
    /**
     * Endpoint: /
     * used to know if the server is up
     */
    @Get()
    @ApiProduces('plain/text')
    @ApiOkResponse({ description: "OK" })
    async Test() {
        return 'OK';
    }
}
