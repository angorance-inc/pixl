import { Module } from '@nestjs/common';
import { AccountController } from './Controllers/account/account.controller';
import { AppController } from './app.controller';
import { ProfileController } from './Controllers/profile/profile.controller';
import { ConfigModule } from './configs/config.module';
import { PublicationController } from './Controllers/publication/publication.controller';
import { WallController } from './Controllers/wall/wall.controller';

@Module({
  imports: [ConfigModule,],
  controllers: [AccountController, AppController, ProfileController, PublicationController, WallController],
  providers: [],
})
export class AppModule { }