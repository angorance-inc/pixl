import { CreateAccountDTO } from "../interfaces/accountsDTO/create-account.dto";
import { LoginRequest } from '../interfaces/accountsDTO/login-body.dto';
import { PublicationDTO } from '../interfaces/publications/publication.dto';
import { UserInfo } from "../interfaces/accountsDTO/user-info.dto";
import { QueryHitDTO } from "../interfaces/publications/query-hit.dto.";
import { profileDTO } from "../interfaces/profilesDTO/profile.dto";
import { AccountEntity } from "../interfaces/database-entity/account.entity";

const bcrypt = require('bcrypt');

const elasticsearch = require('elasticsearch');

const usersCollection = 'users';
const publicationsCollection = 'publications';

const usersType = 'user';
const publicationsType = 'publication';

var db = new elasticsearch.Client({
  host: 'elasticsearch:9200',
  //log: 'trace'
});

class QueryHandler {

  constructor() {

    db.ping({ requestTimeout: 30000 })
      .then(() => console.log('Everything is ok'))
      .catch(() => console.error('elasticsearch cluster is down!'));

    // Users collection creation
    db.indices.exists({ index: usersCollection })
      .then((isExisting) => {
        if (!isExisting) {
          db.indices.create({ index: usersCollection })
            .then((resp) => {
              console.log("create", resp);

              db.indices.putMapping({
                index: usersCollection,
                type: usersType,
                body: {
                  properties: {
                    fName: { type: 'text' },
                    lName: { type: 'text' },
                    email: { type: 'keyword' },
                    username: { type: 'keyword' },
                    password: { type: 'text' },
                    birthdate: { type: 'text' },
                    biography: { type: 'text' },
                    localisation: { type: 'text' },
                    website: { type: 'keyword' },
                    profilePic: { type: 'text' },
                    tagList: { type: 'text' },
                    followingList: { type: 'text' }
                  }
                }
              }, (err, resp, status) => {
                if (err) {
                  console.error(err, status);
                }
                else {
                  console.log('Successfully Created Index', status, resp);
                }
              });
            })
            .catch((err) => console.log(err));
        }
      });

    // Publications collection creation
    db.indices.exists({ index: publicationsCollection })
      .then((isExisting) => {
        if (!isExisting) {
          db.indices.create({ index: publicationsCollection })
            .then((resp) => {
              console.log("create", resp)

              db.indices.putMapping({
                index: publicationsCollection,
                type: publicationsType,
                body: {
                  properties: {
                    creatorID: { type: 'text' },
                    creatorUsername: { type: 'text' },
                    title: { type: 'text' },
                    description: { type: 'text' },
                    tags: { type: 'text' },
                    image: { type: 'text' },
                    publicationDate: { type: 'long' }
                  }
                }
              }, (err, resp, status) => {
                if (err) {
                  console.error(err, status);
                }
                else {
                  console.log('Successfully Created Index', status, resp);
                }
              });
            })
            .catch((err) => console.log(err));
        }
      });
  }

  // ------------------------------------------------------------------
  // Account OPERATIONS -----------------------------------------------

  /**
   * Check if a user exists
   * @param email Email to check
   * @param username Username to check
   */
  DoesUserExist(email: string, username: string): Promise<boolean> {

    const request = {
      index: usersCollection,
      type: usersType,
      body: {
        query: {
          constant_score: {
            filter: {
              bool: {
                should: [
                  { match: { email: email } },
                  { match: { username: username } }
                ]
              }
            }
          }
        }
      }
    }

    return db.search(request)
      .then((result) => {
        console.log(`number of results: ${result.hits.total}`);

        if (result.hits.total > 0) {
          return true;
        }
        return false;
      });
  }

  /**
   * Create one user in the database
   * @param {*} user User to create
   */
  InsertUser(user: CreateAccountDTO): Promise<boolean> {

    const request = {
      index: usersCollection,
      type: usersType,
      refresh: true,
      body: user
    }

    return db.index(request)
      .then((result) => {
        if (result.result === 'created') {
          console.log('User created');
          return true;
        }
        return false;
      });
  }

  /**
   * Login a user
   * @param {*} username 
   * @param {*} password
   */
  Login(user: LoginRequest): Promise<UserInfo> {

    const request = {
      index: usersCollection,
      type: usersType,
      body: {
        query: {
          constant_score: {
            filter: {
              bool: {
                should: [
                  { match: { email: user.username } },
                  { match: { username: user.username } }
                ]
              }
            }
          }
        }
      }
    }

    return db.search(request)
      .then((result) => {
        console.log(`number of results: ${result.hits.total}`);

        if (result.hits.total === 1) {
          let dbHit = result.hits.hits[0]

          if (dbHit._source.username === user.username || dbHit._source.email === user.username) {
            if (bcrypt.compareSync(user.password, dbHit._source.password)) {
              let user: UserInfo = { userID: dbHit._id, username: dbHit._source.username };

              return user;
            }
          }
          return null;
        }
        return null;
      });
  }

  // ------------------------------------------------------------------
  // Wall OPERATIONS --------------------------------------------------

  /**
   * Create one publication in the database
   * @param {*} publication Publication to create
   */
  CreatePublication(publication: PublicationDTO): Promise<boolean> {

    const request = {
      index: publicationsCollection,
      type: publicationsType,
      refresh: true,
      body: publication
    }

    return db.index(request)
      .then((result) => {
        if (result.result === 'created') {
          console.log('Publication created');
          return true;
        }
        return false;
      });
  }

  /**
   * Retrieve all user's publications by his ID
   * @param {*} userID ID of the publications creator 
   */
  GetPublicationsByResearch(page: number, limit: number): Promise<QueryHitDTO<PublicationDTO>[]> {

    const request = {
      index: publicationsCollection,
      type: publicationsType,
      body: {
        sort: [
          { 'publicationDate': { 'order': 'DESC' } }
        ],
        from: (page - 1) * limit,
        size: limit,
        query: {
          "match_all": {}
        }
      }
    }

    return db.search(request)
      .then((result) => {
        console.log(`number of results: ${result.hits.total}`);
        return result.hits.hits;
      }, (err) => {
        return [];
      });
  }

  getRandomUserPublication(userId: string): Promise<string> {
    const request = {
      index: publicationsCollection,
      type: publicationsType,
      body: {
        size: 1,
        query: {
          function_score: {
            query: {
              bool: {
                must: [
                  { match: { creatorID: userId } }
                ]
              }
            },
            functions: [
              {
                random_score: {
                  seed: new Date().getTime()
                }
              }
            ]
          }
        }
      }
    }

    return db.search(request)
      .then((result) => {
        console.log(`number of results: ${result.hits.total}`);
        console.log(result);

        return result.hits.hits[0]._source.image;
      }, (err) => {
        console.log(err)
        return "";
      });
  }

  /**
   * Retrieve all user's publications by his ID
   * @param {*} userID ID of the publications creator 
   */
  GetPublicationsByUserID(userID: string, page: number, limit: number): Promise<QueryHitDTO<PublicationDTO>[]> {

    const request = {
      index: publicationsCollection,
      type: publicationsType,
      body: {
        sort: [
          { 'publicationDate': { 'order': 'DESC' } }
        ],
        from: (page - 1) * limit,
        size: limit,
        query: {
          constant_score: {
            filter: {
              bool: {
                must: [
                  { match: { creatorID: userID } }
                ]
              }
            }
          }
        }
      }
    }

    return db.search(request)
      .then((result) => {
        console.log(`number of results: ${result.hits.total}`);
        return result.hits.hits;
      }, (err) => {
        return [];
      });
  }

  /**
   * Retrieve a publication by its ID
   * @param {*} publicationID ID of the publications 
   */
  getPublicationsById(publicationID: string): Promise<QueryHitDTO<PublicationDTO>[]> {

    const request = {
      index: publicationsCollection,
      type: publicationsType,
      body: {
        query: {
          bool: {
            must: [
              { match: { _id: publicationID } }
            ]
          }
        }
      }
    }

    return db.search(request)
      .then((result) => {
        console.log(`number of results: ${result.hits.total}`);
        return result.hits.hits;
      });
  }


  // ------------------------------------------------------------------
  // profile OPERATIONS --------------------------------------------------

  getUserProfile(userId: string): Promise<AccountEntity> {
    const request = {
      index: usersCollection,
      type: usersType,
      _sourceExclude: ['*.password'],
      body: {
        query: {
          bool: {
            must: [
              { match: { _id: userId } }
            ]
          }
        }
      }
    }

    return db.search(request)
      .then((result) => {
        //console.log(result.hits.hits);
        return result.hits.hits[0]._source;
      });
  }

  /**
   * Update a user's profile. The informations updated are different than the informations
   * provided at the sign up (expect the names)
   * @param userId ID of the user to update
   * @param updateData new informations
   */
  updateProfile(userId: string, updateData: any): Promise<any> {
    const request = {
      index: usersCollection,
      type: usersType,
      refresh: true,
      id: userId,
      body: { doc: updateData }
    }

    return db.update(request)
      .then((result) => {
        //console.log(result);
        if (result.result === 'updated') {
          console.log('User updated');
          return true;
        }
        return false;
      });
  }

}

const queryHandler = new QueryHandler();
export { queryHandler };