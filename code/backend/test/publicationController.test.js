const { expect } = require('chai');
const axios = require('axios');
const fs = require('fs');
const jwt = require('jsonwebtoken')

const client = axios.create({
    baseURL: 'http://localhost:3000',
    timeout: 2000,
});



describe('Post publication', function () {
    var token;
    var userId;
    var username;
    var payload
    before(async () => {
        await client.post('/accounts/registration', { fName: 'baseUser', lName: "test", username: 'baseUser', email: 'base@pixl.ch', password: "password" }).then(() => {
            console.log('user created');
        }, () => {
            console.log('user already existed');
        });

        await client.post('/accounts/authentication', { username: 'baseUser', password: "password" }).then(success => {
            token = success.data.token;
            tokenPayload = jwt.decode(token);
            //console.log(tokenPayload);
            userId = tokenPayload.userID;
            username = tokenPayload.username;

            payload = {
                title: 'Example picture',
                description: 'publication for the unit tests',
                tags: ['tag1', 'tag2'],
                image: 'nogooddatabutdataanyway',
                imageMini: 'supposedtobethesameimagebutmorelittle',
                creatorID: userId,
                creatorUsername: username
            };

            expect(success.status).to.be.eql(201);
        }, err => {
            console.log('no good bud');
            console.log(err);
            expect.fail();
        });

    });


    /**************************************
     * Publications creation 
     **************************************/
    it('should say that the token is invalid', function () {
        return client.post('/publications', payload, { headers: { Authorization: 'badToken' } })
            .then(() => {
                expect.fail('the post has been published without a valid token');
            }, err => {
                expect(err.response.status).to.be.eql(403);
            });
    });

    it('should say that a field is missing', function () {
        let badPayload = {};
        return client.post('/publications', badPayload, { headers: { Authorization: token } })
            .then(() => {
                expect.fail('the post has been published with a missing field');
            }, err => {
                expect(err.response.status).to.be.eql(400);
            });
    });

    it('should create an image in the file system', function () {
        return client.post('/publications', payload, { headers: { Authorization: token } })
            .then(success => {
                expect(success.status).to.be.eql(201);
            }, err => {
                expect.fail('the post hasn\'t been published with good inputs : ' + err.response.data.error);
                //TODO complete with error codes
            });
    });

    /*
        it('should edit a publication', function(){
            return true;
        });
    
        it('should delete a publication', function(){
            return true;
        });
    
        it('should obtain a publication', function(){
            return true;
        });*/

});
