const { expect } = require('chai');
const axios = require('axios');

const client = axios.create({
  baseURL: 'http://localhost:3000/accounts',
  timeout: 2000,
});


describe('Registration/Authentication', function () {

  before(() => {
    return client.post('/registration', { fName: 'baseUser', lName: "test", username: 'baseUser', email: 'base@pixl.ch', password: "password" })
      .then(success => {
        expect(success.status).to.be.eql(201);
      }, err => {
        if (err.response.status != 403) {
          expect.fail(err.response.error)
        }
        expect(err.response.status).to.eql(403);
      });
  });

  /****************************************************************
   * Registration
   ***************************************************************/
  it('Registration: should say that the that a field is missing', function () {
    return client.post('/registration', { fName: 'test', username: 'failUser', email: 'fail@pixl.ch', password: "password" })
      .then(() => { expect.fail("new user created, but it shouldn't !") },
        (err) => {
          expect(err.response.status).to.eql(400);
        });
  });

  it('Registration: should say that the new account has been successfully created', function () {
    return client.post('/registration', { fName: 'Newtest', lName: "succes", username: 'NewSuccess', email: 'success@pixl.ch', password: "password" })
      .then((rep) => {
        //  OK
        expect(rep.status).to.eql(201);
      }, (err) => {
        expect.fail("account not created... " + err.response.data.error);
      });
  });


  it('Registration: should say that this email is already in use', async function () {
    return client.post('/registration', { fName: 'baseUser', lName: "test", username: 'baseUser2', email: 'base@pixl.ch', password: "password" })
      .then((rep) => {
        expect.fail('user created, althought it shouldn\'t');
      }, (err) => {
        expect(err.response.status).to.eql(403);
      });
  });


  /****************************************************************
   * Authentication
   ***************************************************************/
  it('Authentication: should say that the that a field is missing', function () {
    return client.post('/authentication', { username: 'base@pixl.ch' })
      .then(() => { expect.fail("User logged in, without providing all information !") },
        (err) => {
          expect(err.response.status).to.eql(400);
        });
  });

  it('Authentication: should say that the credentials are wrong', function () {
    return client.post('/authentication', { username: 'wrongbase@pixl.ch', password: "password" })
      .then(() => { expect.fail("user logged in with bad credentials !") },
        (err) => {
          expect(err.response.status).to.eql(403);
        });
  });

  it('Authentication: should successfully log a user in with the email', function () {
    return client.post('/authentication', { username: 'base@pixl.ch', password: "password" })
      .then((rep) => {
        expect(rep.status).to.eql(201);
      }, err => {
        expect.fail('Cannot log the user in with good credentials');
      });
  });

  it('Authentication: should successfully log a user in with the username', function () {
    return client.post('/authentication', { username: 'baseUser', password: "password" })
      .then((rep) => {
        expect(rep.status).to.eql(201);
      }, err => {
        expect.fail('Cannot log the user in with good credentials');
      });
  });
})