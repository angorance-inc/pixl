# PixL - Database

## How to launch Elasticsearch

1. Go into the folder `/elasticsearch-6.5.1/bin`
2. Type the command `./elasticsearch` or execute `elasticsearch.bat` (for Windows)
3. Go on `localhost:9200` to see that your server is running

---

## Put mapping configuration on Elasticsearch

1. Prepare your json (exemple : `mapping.json`) to configure the way your data is stored and searched
2. Install curl or use it with Bash (for Windows)
3. Type the command `curl -XPUT localhost:9200/movies -d --data-binary @adresseDeMonMappingJSON -H "content-type: application/JSON"` (exemple: `@mapping.json`) to insert the mapping on the collection `movies` in the server

---

## Put JSON data on Elasticsearch

1. Prepare your json (exemple : `movies_elastic.json`). Elasticsearch has a special specification for the data, please follow the given example
2. Install curl or use it with Bash (for Windows)
3. Type the command `curl -XPUT localhost:9200/_bulk --data-binary @adresseDeMonFichierJSON -H "content-type: application/JSON"` (exemple: `@movies_elastic.json`) to insert the data on the database server

--- 

## See data from Elasticsearch

1. To see the collection structur : `localhost:9200/movies`
2. To see all documents of the `movies` collection : `localhost:9200/movies/_search`

---

## Remove data from Elasticsearch

1. Remove collection : `curl -XDELETE localhost:9200/movies`