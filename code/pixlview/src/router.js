import Vue from 'vue';
import Router from 'vue-router';
import Launch from './pages/LaunchPage.vue';
import LaunchNavbar from './layout/LaunchNavbar.vue';
import FullNavbar from './layout/FullNavbar.vue';
import StarterFooter from './layout/StarterFooter.vue';
import Login from './pages/Login.vue';
import Signup from './pages/Signup.vue';
import WallPage from './pages/WallPage.vue';
import Profile from './pages/Profile.vue';
import Parameters from './pages/Parameters.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      components: {
        default: Launch,
        header: LaunchNavbar,
        footer: StarterFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: 'black' }
      },
      meta: {
        guest: true
      }
    },
    {
      path: '/login',
      name: 'login',
      components: {
        default: Login,
        header: LaunchNavbar,
        footer: StarterFooter
      },
      props: {
        header: { colorOnScroll: 400 }
      },
      meta: {
        guest: true
      }
    },
    {
      path: '/signup',
      name: 'signup',
      components: {
        default: Signup,
        header: LaunchNavbar,
        footer: StarterFooter
      },
      props: {
        header: { colorOnScroll: 400 }
      },
      meta: {
        guest: true
      }
    },
    {
      path: '/wall',
      name: 'wall',
      components: {
        default: WallPage,
        header: FullNavbar,
        footer: StarterFooter
      },
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/profile/*',
      name: 'profile',
      components: {
        default: Profile,
        header: FullNavbar,
        footer: StarterFooter
      },
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/parameters',
      name: 'parameters',
      components: {
        default: Parameters,
        header: FullNavbar,
        footer: StarterFooter
      },
      meta: {
        requiresAuth: true
      }
    }
  ],
  scrollBehavior: to => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  }
  // beforeEach: (to, from, next) => {
  //   if (to.matched.some(record => record.meta.requiresAuth)) {
  //     if (sessionStorage.token == null) {
  //       next({
  //         path: '/login',
  //         params: { nextUrl: to.fullPath }
  //       });
  //     } else {
  //       next();
  //     }
  //   } else if (to.matched.some(record => record.meta.guest)) {
  //     if (sessionStorage.token == null) {
  //       next();
  //     } else {
  //       next({ name: 'wall' });
  //     }
  //   } else {
  //     next();
  //   }
  // }
});
